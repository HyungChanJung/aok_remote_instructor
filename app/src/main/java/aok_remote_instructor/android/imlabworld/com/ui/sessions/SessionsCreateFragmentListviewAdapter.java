package aok_remote_instructor.android.imlabworld.com.ui.sessions;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import java.util.ArrayList;

import aok_remote_instructor.android.imlabworld.com.R;
import aok_remote_instructor.android.imlabworld.com.data.entity.Participant;

public class SessionsCreateFragmentListviewAdapter extends BaseAdapter {
    private ArrayList<Participant> listParticipant = new ArrayList<>();

    public ArrayList<Participant> getListParticipant() {
        return listParticipant;
    }

    private DatePickerDialog.OnDateSetListener dateSetListener;
    private TimePickerDialog.OnTimeSetListener timeSetListener;

    TextView dateTextview;
    TextView hourTextView;
    TextView minuteTextView;
    String dateStr;
    String hourStr, minuteStr;


    public TextView getDateTextview() {
        return dateTextview;
    }

    public TextView getHourTextView() {
        return hourTextView;
    }

    public TextView getMinuteTextView() {
        return minuteTextView;
    }

    @Override
    public int getCount() {
        return listParticipant.size() + 2;
    }

    @Override
    public View getView(int position, View convertView, final ViewGroup parent) {

        final Context context = parent.getContext();

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (position == 0) {

            // 첫줄 : 세션 날짜, 세션 시간
            convertView = inflater.inflate(R.layout.fragment_sessions_listview_session_setting_cell, parent, false);

            dateTextview = convertView.findViewById(R.id.fragment_sessions_create_date_textview);
            dateSetListener = new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                    dateTextview.setText(day + "/" + (month + 1) + "/" + year);
                    dateTextview.setTextColor(Color.BLACK);
                    // 저장은 static 으로 바깥으로 빼내는게 나을듯
                    dateStr = day + "/" + (month + 1) + "/" + year;
                }
            };

            hourTextView = convertView.findViewById(R.id.fragment_sessions_create_hour_textview);
            minuteTextView = convertView.findViewById(R.id.fragment_sessions_create_minute_textview);
            timeSetListener = new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker timePicker, int hour, int minute) {
                    hourTextView.setText(hour+"");
                    minuteTextView.setText(minute+"");
                    hourTextView.setTextColor(Color.BLACK);
                    minuteTextView.setTextColor(Color.BLACK);
                    hourStr = hour+"";
                    minuteStr = minute+"";
                }
            };

            FrameLayout dateFrameLayout = convertView.findViewById(R.id.fragment_sessions_create_date_framelayout);
            dateFrameLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    DatePickerDialog dialog = new DatePickerDialog(context, dateSetListener, 2020, 9, 15);
                    dialog.show();
                }
            });

            TextView dateTextview = convertView.findViewById(R.id.fragment_sessions_create_date_textview);
            dateTextview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    DatePickerDialog dialog = new DatePickerDialog(context, dateSetListener, 2020, 9, 15);
                    dialog.show();
                }
            });

            TextView hourTextview = convertView.findViewById(R.id.fragment_sessions_create_hour_textview);
            hourTextview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    TimePickerDialog dialog = new TimePickerDialog(context, timeSetListener, 12, 0, false);
                    dialog.show();
                }
            });

            TextView minuteTextview = convertView.findViewById(R.id.fragment_sessions_create_minute_textview);
            minuteTextview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    TimePickerDialog dialog = new TimePickerDialog(context, timeSetListener, 12, 0, false);
                    dialog.show();
                }
            });


            return convertView;

        } else if (position == getCount() - 1 ){

            // 마지막 줄 : Add Student
            convertView = inflater.inflate(R.layout.fragment_sessions_listview_add_participant_cell, parent, false);
            return convertView;

        } else {

            // 나머지 : 참여 학생 리스트
            convertView = inflater.inflate(R.layout.fragment_sessions_listview_participant_cell, parent, false);
            return convertView;

        }
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

}
