package aok_remote_instructor.android.imlabworld.com.ui.sessions;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.Timestamp;

import aok_remote_instructor.android.imlabworld.com.R;
import aok_remote_instructor.android.imlabworld.com.data.entity.Participant;
import aok_remote_instructor.android.imlabworld.com.data.entity.Session;
import aok_remote_instructor.android.imlabworld.com.databinding.FragmentSessionsCreateBinding;

public class SessionsCreateFragment extends Fragment {

    // View view;
    private FragmentSessionsCreateBinding binding;

    private ListView sessionCreateListView;
    private SessionsCreateFragmentListviewAdapter sessionsCreateFragmentListviewAdapter;
    private SessionsViewModel sessionsViewModel;
    private Session selectedSession;

    public SessionsCreateFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        Log.e("testt","onDestroy");
        // 이때 싱글턴인 세션을 해제해준다
        // 혹은 Lifecycles를 이용하자
        sessionsViewModel.getSessionRepository().selectedSession = null;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_sessions_create, container, false);
        View root = binding.getRoot();

        //엑티비티에서 뷰모델을 가져와서 세션이 계속 같은 세션이 됨
        sessionsViewModel = ViewModelProviders.of(getActivity()).get(SessionsViewModel.class);
        //프래그먼트를 만들때 마다 뷰모델을 만들어서 세이브 누를때 마다 새로운 세션이 생성됨
        //sessionsViewModel = new ViewModelProvider(this).get(SessionsViewModel.class);

        sessionCreateListView = binding.fragmentSessionsCreateListview;
        sessionsCreateFragmentListviewAdapter = new SessionsCreateFragmentListviewAdapter();
        sessionsCreateFragmentListviewAdapter.getListParticipant().add(new Participant());

        sessionCreateListView.setAdapter(sessionsCreateFragmentListviewAdapter);
        sessionCreateListView.setOnItemClickListener(settingsOnItemClickListener);

        if(sessionsViewModel.getSessionRepository().selectedSession != null) {
            selectedSession = sessionsViewModel.getSessionRepository().selectedSession;
        }



        final ImageView backArrow = binding.fragmentSessionsCreateBackArrow;
        backArrow.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                NavDirections action = SessionsCreateFragmentDirections.actionSessionsCreateFragmentToNavigationSessions();
                Navigation.findNavController(v).navigate(action);
            }

        });

        final TextView save = binding.fragmentSessionsCreateSave;
        save.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if(sessionsCreateFragmentListviewAdapter.dateStr == null) {
//                    Toast.makeText(getContext(), "날짜를 입력해주세요", Toast.LENGTH_SHORT).show();
                    sessionsCreateFragmentListviewAdapter.getDateTextview().setError("날짜를 입력해주세요");
                    return;
                } else if (sessionsCreateFragmentListviewAdapter.hourStr == null) {
//                    Toast.makeText(getContext(), "시간을 입력해주세요", Toast.LENGTH_SHORT).show();
                    sessionsCreateFragmentListviewAdapter.getMinuteTextView().setError("시간을 입력해주세요");
                    return;
                }

                // 추후 변경필요
                Timestamp timestamp = Timestamp.now();
                sessionsViewModel.createSession(timestamp);
                NavDirections action = SessionsCreateFragmentDirections.actionSessionsCreateFragmentToSessionsEditFragment();
                Navigation.findNavController(v).navigate(action);
            }

        });

        return root;
    }



    private AdapterView.OnItemClickListener settingsOnItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View v, int i, long l) {

            if ( i == sessionsCreateFragmentListviewAdapter.getCount() - 1) {

                sessionsCreateFragmentListviewAdapter.getListParticipant().add(new Participant());
                sessionsCreateFragmentListviewAdapter.notifyDataSetChanged();
            }

        }
    };

}