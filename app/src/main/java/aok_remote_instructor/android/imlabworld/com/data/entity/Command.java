package aok_remote_instructor.android.imlabworld.com.data.entity;

public class Command {
    String sessionId;
    String studentId;
    int content;

    public Command(String sessionId, String studentId, int content) {
        this.sessionId = sessionId;
        this.studentId = studentId;
        this.content = content;
    }

    public Command() {}

    public String getSessionId() {
        return sessionId;
    }

    public String getStudentId() {
        return studentId;
    }

    public int getContent() {
        return content;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public void setContent(int content) {
        this.content = content;
    }
}
