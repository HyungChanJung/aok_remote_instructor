package aok_remote_instructor.android.imlabworld.com.data.entity;

public class StudentState {
    String studentId;
    String state;
    String depth;
    String cpm;

    public StudentState(String studentId, String state, String depth, String cpm) {
        this.studentId = studentId;
        this.state = state;
        this.depth = depth;
        this.cpm = cpm;
    }

    public StudentState () {}

    public String getStudentId() {
        return studentId;
    }

    public String getState() {
        return state;
    }

    public String getDepth() {
        return depth;
    }

    public String getCpm() {
        return cpm;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public void setState(String state) {
        this.state = state;
    }

    public void setDepth(String depth) {
        this.depth = depth;
    }

    public void setCpm(String cpm) {
        this.cpm = cpm;
    }
}
