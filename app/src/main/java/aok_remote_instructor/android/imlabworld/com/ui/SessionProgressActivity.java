package aok_remote_instructor.android.imlabworld.com.ui;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.SetOptions;
import com.remotemonster.sdk.Config;
import com.remotemonster.sdk.RemonCall;
import com.remotemonster.sdk.RemonCast;
import com.remotemonster.sdk.RemonClientData;
import com.remotemonster.sdk.RemonConference;
import com.remotemonster.sdk.RemonException;
import com.remotemonster.sdk.RemonParticipant;
import com.remotemonster.sdk.data.CloseType;

import org.webrtc.SurfaceViewRenderer;

import java.util.Timer;
import java.util.TimerTask;

import aok_remote_instructor.android.imlabworld.com.BaseActivity;
import aok_remote_instructor.android.imlabworld.com.R;
import aok_remote_instructor.android.imlabworld.com.data.entity.Participant;
import aok_remote_instructor.android.imlabworld.com.data.entity.RemonID;

public class SessionProgressActivity extends BaseActivity {

    public static final String EXTRA_SESSION_ID = "session_id";
    public final String TAG = "SessionProgressActivity";

    private DocumentReference mPostReference;
    private String mSessionId;

    private Context context;

    private String mRoomName;
    private RemonException mError;
    private RemonConference mConference = new RemonConference();


    private FirebaseFirestore db = FirebaseFirestore.getInstance();


    //participant lists
    private Participant[] participantArray = new Participant[6];
    private String[] participantIdArray = new String[6];

    //cells
    private TextView[] tvStates = new TextView[6];
    private TextView[] tvNames = new TextView[6];
    private TextView[] tvCpms = new TextView[6];
    private TextView[] tvDepths = new TextView[6];
    private FrameLayout[] flBorders = new FrameLayout[6];
    private FrameLayout[] flBackgrounds = new FrameLayout[6];
    private Boolean[] mAvailableView = new Boolean[6];
    private ProgressBar[] progressBars = new ProgressBar[6];

    boolean canConnect = true;

    //views
    SurfaceViewRenderer localVideoView, remoteVideoView, localVideoViewHidden;

    //remon
    private RemonCast remonCast = null;
    private RemonCall remonCall = null;


    boolean isMuted = false;
    boolean isVideoOff = false;

    // timeer
    boolean isTimerActivated;
    TimerTask timerTask;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_session_progress);

        context = this;

        mAvailableView[0] = false;
        mAvailableView[1] = false;
        mAvailableView[2] = false;
        mAvailableView[3] = false;
        mAvailableView[4] = false;
        mAvailableView[5] = false;

        tvStates[0] = findViewById(R.id.cell1_tv_state);
        tvStates[1] = findViewById(R.id.cell2_tv_state);
        tvStates[2] = findViewById(R.id.cell3_tv_state);
        tvStates[3] = findViewById(R.id.cell4_tv_state);
        tvStates[4] = findViewById(R.id.cell5_tv_state);
        tvStates[5] = findViewById(R.id.cell6_tv_state);

        tvNames[0] = findViewById(R.id.cell1_tv_name);
        tvNames[1] = findViewById(R.id.cell2_tv_name);
        tvNames[2] = findViewById(R.id.cell3_tv_name);
        tvNames[3] = findViewById(R.id.cell4_tv_name);
        tvNames[4] = findViewById(R.id.cell5_tv_name);
        tvNames[5] = findViewById(R.id.cell6_tv_name);

        tvCpms[0] = findViewById(R.id.cell1_tv_cpm);
        tvCpms[1] = findViewById(R.id.cell2_tv_cpm);
        tvCpms[2] = findViewById(R.id.cell3_tv_cpm);
        tvCpms[3] = findViewById(R.id.cell4_tv_cpm);
        tvCpms[4] = findViewById(R.id.cell5_tv_cpm);
        tvCpms[5] = findViewById(R.id.cell6_tv_cpm);

        tvDepths[0] = findViewById(R.id.cell1_tv_depth);
        tvDepths[1] = findViewById(R.id.cell2_tv_depth);
        tvDepths[2] = findViewById(R.id.cell3_tv_depth);
        tvDepths[3] = findViewById(R.id.cell4_tv_depth);
        tvDepths[4] = findViewById(R.id.cell5_tv_depth);
        tvDepths[5] = findViewById(R.id.cell6_tv_depth);

        flBorders[0] = findViewById(R.id.cell1_fl_border);
        flBorders[1] = findViewById(R.id.cell2_fl_border);
        flBorders[2] = findViewById(R.id.cell3_fl_border);
        flBorders[3] = findViewById(R.id.cell4_fl_border);
        flBorders[4] = findViewById(R.id.cell5_fl_border);
        flBorders[5] = findViewById(R.id.cell6_fl_border);

        flBackgrounds[0] = findViewById(R.id.cell1_fl_bg);
        flBackgrounds[1] = findViewById(R.id.cell2_fl_bg);
        flBackgrounds[2] = findViewById(R.id.cell3_fl_bg);
        flBackgrounds[3] = findViewById(R.id.cell4_fl_bg);
        flBackgrounds[4] = findViewById(R.id.cell5_fl_bg);
        flBackgrounds[5] = findViewById(R.id.cell6_fl_bg);

        progressBars[0] = findViewById(R.id.cell1_pb);
        progressBars[1] = findViewById(R.id.cell2_pb);


        // init VideoViews
        localVideoView = findViewById(R.id.localVideo);
        localVideoView.setMirror(true);

        localVideoViewHidden = findViewById(R.id.localVideoHidden);
        localVideoViewHidden.setMirror(true);

        remoteVideoView = findViewById(R.id.remoteVideo);
        remoteVideoView.setMirror(true);



        // Get session key from intent
        mSessionId = getIntent().getStringExtra(EXTRA_SESSION_ID);
        if (mSessionId == null) {
            throw new IllegalArgumentException("Must pass EXTRA_SESSION_ID");
        } else {
            Log.e("testt","SESSION ID : " + mSessionId);
            mRoomName = mSessionId;
        }

//        initRemonConference();
        initRemonCast();


        // Initialize Database
        // mPostReference = FirebaseFirestore.getInstance().collection("sessions").document(mSessionId);


        // 각 SurfaceViewRenderer에 (총 6개) 각 학생들의 센서정보를 listen

        // intent 로 넘어온 정보를 바탕으로 firestore의 해당 세션의 학생 정보 받아옴
        // 받아온 정보를 mParticipantIds, mParticipants 에 저장해 관리
        // switch 문을 거치면서 학생이 추가, 변경, 삭제되는 상태에 대해 대응

        db.collection("sessions")
                .document(mSessionId)
                .collection("participants")
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot snapshots,
                                        @Nullable FirebaseFirestoreException e) {
                        if (e != null) {
                            Log.w("testt", "listen:error", e);
                            return;
                        }

                        String participantKey;
                        int participantIndex;
                        Participant participant;

                        for (DocumentChange dc : snapshots.getDocumentChanges()) {
                            switch (dc.getType()) {
                                case ADDED:
                                    // for 문 돌려서 가능한 자리를 찾는다
                                    int index = getAvailableCellCount();

                                    if (index < 0 || index > 6) {
                                        dc.getDocument().getReference().delete();
                                        Toast.makeText(context, "최대 입장인원을 초과하여 학생의 입장을 거절했습니다.", Toast.LENGTH_SHORT).show();
                                        return;
                                    }

                                    // 찾은 자리의 participant array 에 값을 넣는다
                                    participantKey = dc.getDocument().getId();
                                    participantIdArray[index] = participantKey;

                                    participant = dc.getDocument().toObject(Participant.class);
                                    participantArray[index] = participant;

                                    Log.d("testt", "New participant: " + dc.getDocument().getData());
                                    // 뷰를 업데이트해준다
                                    tvStates[index].setText("Ready");
                                    tvNames[index].setText(participant.getName());
                                    break;

                                case MODIFIED:
                                    participant = dc.getDocument().toObject(Participant.class);
                                    participantKey = dc.getDocument().getId();
                                    participantIndex = getIndexOfParticipant(participantKey);

                                    if (participantIndex > -1 && participantIndex < 6) {
                                        participantArray[participantIndex] = participant;
                                        tvStates[participantIndex].setText(participant.getState());

                                        if(participant.getState() != null) {
                                            if (participant.getState().equals("Good")){
                                                tvStates[participantIndex].setTextColor(getResources().getColor(R.color.green_100));
                                                flBorders[participantIndex].setBackground(getResources().getDrawable(R.drawable.radius3_green_boarder));
                                            } else {
                                                tvStates[participantIndex].setTextColor(getResources().getColor(R.color.colorPrimary));
                                                flBorders[participantIndex].setBackground(getResources().getDrawable(R.drawable.radius3_hs100_boarder));
                                            }
                                        }

                                        if(participant.getName() != null) {
                                            tvNames[participantIndex].setText(participant.getName());
                                        }

                                        if(participant.getCpm() != null){
                                            tvCpms[participantIndex].setText(participant.getCpm() + " CPM");
                                        }

                                        if(participant.getDepth() != null){
                                            tvDepths[participantIndex].setText(participant.getDepth() + " cm");
                                        }

                                    } else {
                                        Log.w("testt", "onChildChanged:unknown_child:" + participantKey);
                                    }
                                    Log.d("testt", "Modified participant: " + dc.getDocument().getData());
                                    break;

                                case REMOVED:
                                    participantKey = dc.getDocument().getId();
                                    participantIndex = getIndexOfParticipant(participantKey);

                                    if (participantIndex > -1 && participantIndex < 6) {
                                        participantIdArray[participantIndex] = null;
                                        participantArray[participantIndex] =  null;

                                        tvStates[participantIndex].setText("Leave");
                                        tvStates[participantIndex].setTextColor(getResources().getColor(R.color.black_100));
                                        flBorders[participantIndex].setBackground(null);
                                        tvNames[participantIndex].setText("-");
                                        tvCpms[participantIndex].setText("0 CPM");
                                        tvDepths[participantIndex].setText("0.0 cm");

                                        if(remonCall != null)
                                            remonCall.close();
                                    } else {
                                        Log.w("testt", "onChildRemoved:unknown_child:" + participantKey);
                                    }
                                    Log.d("testt", "Removed participant: " + dc.getDocument().getData());
                                    break;
                            }
                        }

                    }
                });

        TextView endSessionTextView = findViewById(R.id.activity_session_progress_endsession_textview);
        endSessionTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private int getAvailableCellCount() {

        for (int i = 0 ; i < participantIdArray.length ; i++ ) {

            if (participantIdArray[i] == null) {

                return i;

            }

        }

        return -1;
    }

    private int getIndexOfParticipant(String id) {

        for (int i = 0 ; i < participantIdArray.length ; i++ ) {

            if (participantIdArray[i] != null) {
                if (participantIdArray[i].equals(id)) {

                    return i;

                }
            }


        }

        return -1;
    }


    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
//        mConference.leave();
//        db.collection("sessions")
//                .document(mSessionId).delete();
//        remonCast.close();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        mConference.leave();
//        db.collection("sessions")
//                .document(mSessionId).delete();
        remonCast.close();
    }

    private void initRemonCast() {

        remonCast = new RemonCast();

        Config config = new Config();
        config.setVideoCodec("VP8");
        config.setVideoWidth(640);
        config.setVideoHeight(480);
        config.setVideoFps(30);
        config.setStartVideoBitrate(1000);
        config.setAudioStartBitrate(32);
        config.setVideoCall(true);
        config.setLogLevel(Log.INFO);
        config.setKey("43b95e95900daa5d6b1e551e1dfaa5987220e4b4c13c94e1d2f04a104bf874c8");
        config.setServiceId("f8ab9f51-a99b-4460-ba9c-599ee8845a1b");
        config.setLocalView(localVideoView);
        config.setContext(SessionProgressActivity.this);

        remonCast.create(mSessionId, config);

        remonCast.onCreate(new RemonCast.OnCreateCallback() {
            @Override
            public void onCreate(String id) {
                Log.d("testt","remonCast.getId()"+remonCast.getId());
                Log.d("testt","remonCast.getChannelId()"+remonCast.getChannelId());
                RemonID remonID = new RemonID();
                remonID.setRemonID(remonCast.getChannelId());
                db.collection("sessions")
                        .document(mSessionId).set(remonID, SetOptions.merge());
            }
        });
    }


    public void onMuteClicked(View v){

        PopupMenu popup = new PopupMenu(this, v);//v는 클릭된 뷰를 의미

        getMenuInflater().inflate(R.menu.class_setting_menu, popup.getMenu());

        if(!isMuted)
            popup.getMenu().getItem(0).setTitle("음소거 실행");
        else
            popup.getMenu().getItem(0).setTitle("음소거 취소");

        if(!isVideoOff)
            popup.getMenu().getItem(1).setTitle("영상송출 끄기");
        else
            popup.getMenu().getItem(1).setTitle("영상송출 켜기");

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.m1:
                        if(remonCast != null) {
                            if (!isMuted) {
                                remonCast.setLocalAudioEnabled(false);
                                Toast.makeText(context, "음소거가 실행되었습니다.", Toast.LENGTH_SHORT).show();
                                isMuted = true;
                            } else {
                                remonCast.setLocalAudioEnabled(true);
                                Toast.makeText(context, "음소거가 취소되었습니다.", Toast.LENGTH_SHORT).show();
                                isMuted = false;
                            }

                        }
                        break;
                    case R.id.m2:
                        if(remonCast != null) {
                            if (!isVideoOff) {
                                remonCast.setLocalVideoEnabled(false);
                                Toast.makeText(context, "내 영상을 껐습니다.", Toast.LENGTH_SHORT).show();
                                isVideoOff = true;
                            } else {
                                remonCast.setLocalVideoEnabled(true);
                                Toast.makeText(context, "내 영상을 켰습니다.", Toast.LENGTH_SHORT).show();
                                isVideoOff = false;
                            }

                        }
                        break;
                }
                return false;
            }
        });
        popup.show();


    }


    public void onCell1Clicked(View v){
        connectCall(0);
    }
    public void onCell2Clicked(View v) {
        connectCall(1);
    }
    public void onCell3Clicked(View v) {
        connectCall(2);
    }
    public void onCell4Clicked(View v) {
        connectCall(3);
    }
    public void onCell5Clicked(View v) {
        connectCall(4);
    }
    public void onCell6Clicked(View v) {
        connectCall(5);
    }

    private void setCellBackgroundYellow(int index) {
        if(participantIdArray[index] == null)
            return;

        for (int i = 0 ; i < flBackgrounds.length ; i++ ) {
            if (i == index) {
                flBackgrounds[i].setBackground(getResources().getDrawable(R.drawable.radius3_white_boarder_fill_yellow));
            } else {
                flBackgrounds[i].setBackground(null);
            }
        }
    }

    private void connectCall(int index) {

        //여기서 5초지났는지 채크값을 확인해 안지났으면 리턴해버린다.


        try {
            if(canConnect == false)
                return;

            if(participantIdArray[index] == null)
                return;

            setCellBackgroundYellow(index);

            progressBars[index].setVisibility(View.VISIBLE);

            canConnect = false;
            // 5초뒤 모든 pb gone
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try{
                        Thread.sleep(5000);
                    }catch (Exception e){

                    }

                    runOnUiThread(()->{
                        progressBars[0].setVisibility(View.GONE);
                        progressBars[1].setVisibility(View.GONE);
                    });
                    canConnect = true;
                }
            }).start();


            if (remonCall != null) {
                remonCall.close();
                remonCall = null;
            }
            remonCall = new RemonCall();
            Config config = new Config();
            config.setKey("43b95e95900daa5d6b1e551e1dfaa5987220e4b4c13c94e1d2f04a104bf874c8");
            config.setServiceId("f8ab9f51-a99b-4460-ba9c-599ee8845a1b");
            config.setLocalView(localVideoViewHidden);
            config.setRemoteView(remoteVideoView);
            config.setActivity(this);
            if (participantArray[index] != null) {
                String connectChId = participantArray[index].getStudentId();
                remonCall.connect(connectChId, config);

                RemonID remonID = new RemonID();
                remonID.setIsCalled("true");

                db.collection("sessions")
                        .document(mSessionId)
                        .collection("participants")
                        .document(participantIdArray[index])
                        .set(remonID, SetOptions.merge());
            }

            remonCall.onClose(new RemonCall.OnCloseCallback() {
                @Override
                public void onClose(CloseType closeType) {
//                Toast.makeText(context, "연결된 통화를 종료합니다.", Toast.LENGTH_SHORT).show();
                    Log.e("testt","onClose " + closeType);
                }
            });

            remonCall.onConnect(new RemonCall.OnConnectCallback() {
                @Override
                public void onConnect(String chid) {
                    Log.e("testt","onConnect " + chid);

                }
            });

            remonCall.onError(new RemonClientData.OnErrorCallback() {
                @Override
                public void onError(RemonException e) {
                    Log.e("testt","onError " + e);
                }
            });


        } catch (Exception e) {
            Toast.makeText(context, "사용자 전환중 오류가 발생했습니다." + e, Toast.LENGTH_SHORT).show();
            Log.e("testt","error : " + e);
        }

    }

    public void onStartLoggingClicked(View v) {
        final CharSequence[] items = {"1 minute", "2 minutes", "3 minutes", "Unlimited (manual)"};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);     // 여기서 this는 Activity의 this
        final int[] selectedIndex = {0};

        TextView tvStartLogging = findViewById(R.id.activiy_session_progress_start_logging_textview);
        LinearLayout llStartLogging = findViewById(R.id.activiy_session_progress_start_logging_linearlayout);
        ImageView ivStartLogging = findViewById(R.id.activiy_session_progress_start_logging_imageview);
        Timer timer = new Timer();

        if(isTimerActivated) {
            tvStartLogging.setText("Start logging");
            tvStartLogging.setTextColor(getResources().getColor(R.color.black_60));
            llStartLogging.setBackground(getResources().getDrawable(R.drawable.radius100_black_30_boarder));
            ivStartLogging.setImageResource(R.drawable.ic_assessment_30dp);
            timerTask.cancel();
            timerTask = null;
            isTimerActivated = false;
            return;
        }

        // 여기서 부터는 알림창의 속성 설정
        builder.setTitle("Logging settings")        // 제목 설정
                .setSingleChoiceItems(items, -1,  new DialogInterface.OnClickListener(){    // 목록 클릭시 설정
                    public void onClick(DialogInterface dialog, int which){
                        selectedIndex[0] = which;
                    }
                }).setPositiveButton("Start", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // User clicked OK, so save the selectedItems results somewhere
                        // or return them to the component that opened the dialog



                        isTimerActivated = true;

                        Toast.makeText(getApplicationContext(), "선택한 id : " + selectedIndex[0], Toast.LENGTH_SHORT).show();
                        llStartLogging.setBackground(getResources().getDrawable(R.drawable.radius100_gray_boarder_fill_red));
                        ivStartLogging.setImageResource(R.drawable.radius2_white_rectangle);
                        tvStartLogging.setTextColor(getResources().getColor(R.color.white));
                        if(timerTask != null)
                        {
                            timerTask.cancel();
                            timerTask = null;
                        }
                        timerTask = new TimerTask()
                        {
                            int count = (selectedIndex[0]+1) * 60;

                            @Override
                            public void run()
                            {
                                count--;
                                tvStartLogging.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        tvStartLogging.setText(count + " sec");
                                    }
                                });

                                if(count < 1) {
                                    timerTask.cancel();
                                    timerTask = null;
                                    runOnUiThread(()->{
                                        tvStartLogging.setText("Start logging");
                                        tvStartLogging.setTextColor(getResources().getColor(R.color.black_60));
                                        llStartLogging.setBackground(getResources().getDrawable(R.drawable.radius100_black_30_boarder));
                                        ivStartLogging.setImageResource(R.drawable.ic_assessment_30dp);
                                    });
                                }
                            }
                        };
                        timer.schedule(timerTask,0 ,1000);

                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(getApplicationContext(), "취소", Toast.LENGTH_SHORT).show();
                    }
                });

        AlertDialog dialog = builder.create();    // 알림창 객체 생성
        dialog.show();    // 알림창 띄우기
    }


    /*private void initRemonConference() {
        mError = null;


        // 공통적으로 사용될 기본 설정
        Config config  = new Config();
        config.context = this;
//        config.serviceId="SERVICEID1";
//        config.key="1234567890";
        config.serviceId="f8ab9f51-a99b-4460-ba9c-599ee8845a1b";
        config.key="43b95e95900daa5d6b1e551e1dfaa5987220e4b4c13c94e1d2f04a104bf874c8";


        // 컨퍼런스를 위한 마스터 객체를 생성합니다.
        mConference.create ( mRoomName, config, new RemonConference.OnEventCallback() {

            @Override
            public void onEvent(RemonParticipant participant) {
                // 마스터 유저(송출자,나자신) 초기화
                participant.getConfig().localView = localVideoView;
                participant.getConfig().remoteView = null;





                // 뷰 설정
                //mAvailableView[0] = true;

            }
        }).on("onRoomCreated", new RemonConference.OnEventCallback() {

            @Override
            public void onEvent(RemonParticipant participant) {
                // 마스터 유저가 접속된 이후에 호출(실제 송출 시작)
                // 실제 유저 정보는 각 서비스에서 관리하므로, 서비스에서 채널과 실제 유저 매핑 작업 진행

                // firestore 의 session 에 instructor 의 remonID값 저장, 이를 보고 학생은 강사의 비디오만 받아온다.
                // 또한 해당 값이 있을때만 접속한다.
                String instructorRemonID = participant.id;
                RemonID remonID = new RemonID();
                remonID.setRemonID(instructorRemonID);
                db.collection("sessions")
                        .document(mSessionId).set(remonID, SetOptions.merge());


                // tag 객체에 holder 형태로 객체를 지정해 사용할 수 있습니다.
                // 예제에서는 단순히 view의 index를 저장합니다.
                participant.tag = 0;
                Toast.makeText(context, "ch:" + participant.id, Toast.LENGTH_SHORT).show();
            }
        }).on("onUserJoined", new RemonConference.OnEventCallback() {

            @Override
            public void onEvent(RemonParticipant participant) {
                // 다른 사용자가 입장한 경우 초기화를 위해 호출됨
                // 실제 유저 매핑 : it.id 값으로 연결된 실제 유저를 얻습니다.


                // 뷰 설정
                int index = getAvailableView();
                if( index > -1 ) {
                    participant.getConfig().localView = null;
//                    participant.getConfig().remoteView = svRenderers[index];
                    participant.tag = index;
                }


                Toast.makeText(context, participant.id + "참여", Toast.LENGTH_SHORT).show();
//                updateViews();
            }
        }).on("onUserConnected",new RemonConference.OnEventCallback(){

            @Override
            public void onEvent(RemonParticipant participant) {

            }
        }).on("onUserLeft", new RemonConference.OnEventCallback() {

            @Override
            public void onEvent(RemonParticipant participant) {
                // 다른 사용자가 퇴장한 경우
                // it.id 와 it.tag 를 참조해 어떤 사용자가 퇴장했는지 확인후 퇴장 처리를 합니다.
                int index = (int)participant.tag;

                mAvailableView[index] = false;

                if( participant.getLatestError() != null) {
                    // 에러로 인해 종료된 경우
                    // 재시도나 에러 메시지 표시 등 서비스에서 구현
                }

                Toast.makeText(context, participant.id + "퇴장", Toast.LENGTH_SHORT).show();

//                updateViews();

            }
        }).close(new RemonConference.OnConferenceCloseCallback(){

            @Override
            public void onConferenceClose() {
                // 마스터유저가 끊어진 경우 호출됩니다.
                // 송출이 중단되면 그룹통화에서 끊어진 것이므로, 다른 유저와의 연결도 모두 끊어집니다.
                if(mError != null ) {
                    // 에러로 종료됨
                } else {
                    // 종료됨
                }
                Log.d(TAG, "onClose");
            }
        }).error(e -> {
            // 송출 채널의 오류 발생시 호출됩니다.
            // 오류로 연결이 종료되면 error -> close 순으로 호출됩니다.
            mError = e;
            Log.e(TAG, "error=" + e.getDescription());
            Toast.makeText(context, "error="+e.getErrorCode()+",msg="+ e.getDescription(), Toast.LENGTH_SHORT).show();
        });
    }

    private int getAvailableView() {
        for( int i = 0 ; i < mAvailableView.length ; i++) {
            if(!mAvailableView[i]) {
                mAvailableView[i] = true;
                return i;
            }
        }
        return -1;
    }*/


}