package aok_remote_instructor.android.imlabworld.com.data.entity;

public class SessionData {
    String id;
    String studentId;
    String studentName;
    String sessionId;
    String sessionTime;
    String compressionScore;
    String averageDepth;
    String recoil;
    String breathScore;
    String averageVolume;
    String flowFractionScore;
    String handsOffTime;
    String totalScore;
    String graph;

    public SessionData(String id, String studentId, String studentName, String sessionId, String sessionTime, String compressionScore, String averageDepth, String recoil, String breathScore, String averageVolume, String flowFractionScore, String handsOffTime, String totalScore, String graph) {
        this.id = id;
        this.studentId = studentId;
        this.studentName = studentName;
        this.sessionId = sessionId;
        this.sessionTime = sessionTime;
        this.compressionScore = compressionScore;
        this.averageDepth = averageDepth;
        this.recoil = recoil;
        this.breathScore = breathScore;
        this.averageVolume = averageVolume;
        this.flowFractionScore = flowFractionScore;
        this.handsOffTime = handsOffTime;
        this.totalScore = totalScore;
        this.graph = graph;
    }

    public SessionData() {}

    public String getId() {
        return id;
    }

    public String getStudentId() {
        return studentId;
    }

    public String getStudentName() {
        return studentName;
    }

    public String getSessionId() {
        return sessionId;
    }

    public String getSessionTime() {
        return sessionTime;
    }

    public String getCompressionScore() {
        return compressionScore;
    }

    public String getAverageDepth() {
        return averageDepth;
    }

    public String getRecoil() {
        return recoil;
    }

    public String getBreathScore() {
        return breathScore;
    }

    public String getAverageVolume() {
        return averageVolume;
    }

    public String getFlowFractionScore() {
        return flowFractionScore;
    }

    public String getHandsOffTime() {
        return handsOffTime;
    }

    public String getTotalScore() {
        return totalScore;
    }

    public String getGraph() {
        return graph;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public void setSessionTime(String sessionTime) {
        this.sessionTime = sessionTime;
    }

    public void setCompressionScore(String compressionScore) {
        this.compressionScore = compressionScore;
    }

    public void setAverageDepth(String averageDepth) {
        this.averageDepth = averageDepth;
    }

    public void setRecoil(String recoil) {
        this.recoil = recoil;
    }

    public void setBreathScore(String breathScore) {
        this.breathScore = breathScore;
    }

    public void setAverageVolume(String averageVolume) {
        this.averageVolume = averageVolume;
    }

    public void setFlowFractionScore(String flowFractionScore) {
        this.flowFractionScore = flowFractionScore;
    }

    public void setHandsOffTime(String handsOffTime) {
        this.handsOffTime = handsOffTime;
    }

    public void setTotalScore(String totalScore) {
        this.totalScore = totalScore;
    }

    public void setGraph(String graph) {
        this.graph = graph;
    }
}
