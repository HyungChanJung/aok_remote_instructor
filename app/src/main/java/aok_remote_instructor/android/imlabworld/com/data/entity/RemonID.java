package aok_remote_instructor.android.imlabworld.com.data.entity;

public class RemonID {
    public RemonID () {}

    String remonID;
    String isCalled;

    public String getRemonID() {
        return remonID;
    }

    public String getIsCalled() {
        return isCalled;
    }

    public void setRemonID(String remonID) {
        this.remonID = remonID;
    }

    public void setIsCalled(String isCalled) {
        this.isCalled = isCalled;
    }
}
