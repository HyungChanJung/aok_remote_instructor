package aok_remote_instructor.android.imlabworld.com.ui.sessions;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.Timestamp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import aok_remote_instructor.android.imlabworld.com.MainActivity;
import aok_remote_instructor.android.imlabworld.com.R;
import aok_remote_instructor.android.imlabworld.com.SignInActivity;
import aok_remote_instructor.android.imlabworld.com.data.entity.Session;
import aok_remote_instructor.android.imlabworld.com.databinding.FragmentSessionsBinding;
import aok_remote_instructor.android.imlabworld.com.ui.SessionProgressActivity;

public class SessionsFragment extends Fragment {

    public static final String EXTRA_SESSION_ID = "session_id";

    private SessionsViewModel sessionsViewModel;
    private FragmentSessionsBinding binding;
    private FirebaseFirestore db;
    private SessionAdapter mAdapter;
    private RecyclerView mRecycler;
    private String incomingSessionId;
    SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy | hh:mm aa");

    public View onCreateView(@NonNull final LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        sessionsViewModel = ViewModelProviders.of(getActivity()).get(SessionsViewModel.class);

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_sessions, container, false);
        View root = binding.getRoot();

        db = sessionsViewModel.getSessionRepository().getDb();

        mRecycler = binding.fragmentSessionsSessionsRecyclerview;
        mRecycler.setHasFixedSize(true);

        Query postsQuery = getQuery(db);
        mAdapter = new SessionAdapter(postsQuery);
        mRecycler.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecycler.setAdapter(mAdapter);


        final TextView textView = binding.textHome;
        sessionsViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });

        final CardView cardView = binding.fragmentSessionsStartCardview;
        /*cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("testt","clicked" + incomingSessionId);
                Intent intent = new Intent(getActivity(), SessionProgressActivity.class);
                intent.putExtra(EXTRA_SESSION_ID, incomingSessionId);
                startActivity(intent);
            }
        });*/

        final FrameLayout frameLayout = binding.fragmentSessionsStartFragment;
        frameLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("testt","clicked" + incomingSessionId);
                Intent intent = new Intent(getActivity(), SessionProgressActivity.class);
                intent.putExtra(EXTRA_SESSION_ID, incomingSessionId);
                startActivity(intent);
            }
        });

        final TextView incomingSessionTextview = binding.fragmentSessionsIncomingSessionTextview;



        Calendar cal90minBefore = Calendar.getInstance();
        cal90minBefore.setTime(Calendar.getInstance().getTime());
        cal90minBefore.add(Calendar.MINUTE,-90);
        Timestamp ts90minBefore = new Timestamp(cal90minBefore.getTime());

        Calendar cal30minAfter = Calendar.getInstance();
        cal30minAfter.setTime(Calendar.getInstance().getTime());
        cal30minAfter.add(Calendar.MINUTE,30);
        Timestamp ts30minAfter = new Timestamp(cal30minAfter.getTime());


        db.collection("sessions")
                .whereGreaterThan("time", ts90minBefore)
                .whereLessThan("time",ts30minAfter)
                .orderBy("time", Query.Direction.ASCENDING).limit(1).get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {

                            if (task.getResult().isEmpty()) {

                                Log.d("testt", "session info : no session");
                                return;
                            }

                            for (QueryDocumentSnapshot document : task.getResult()) {

                                LinearLayout linearLayout = binding.fragmentSessionsNoSessionLinearlayout;
                                linearLayout.setVisibility(View.GONE);
                                cardView.setVisibility(View.VISIBLE);


                                Log.d("testt", "session info : " + document);

                                Session session = document.toObject(Session.class);
                                incomingSessionId = session.getId();

                                String string = "Session ID : " + session.getId() + "\n"
                                        + "Instructor ID : " + session.getInstructorId() + "\n"
                                        + sdf.format(session.getTime().toDate());
                                incomingSessionTextview.setText(string);

                            }
                        } else {
                            Log.d("testt", "No session is planned for now");
                        }
                    }
                });

        final Button createSessionButton = binding.fragmentSessionsCreateSessionButton;
        createSessionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createSession(v);
            }
        });


        final FloatingActionButton createSessionFAB = binding.fragmentSessionsCreateSessionFab;
        createSessionFAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createSession(v);
            }
        });

        return root;
    }

    private void createSession(View v) {
        NavDirections action = SessionsFragmentDirections.actionNavigationSessionsToSessionsCreateFragment();
        Navigation.findNavController(v).navigate(action);
    }

    private void updateSession(View v) {
        NavDirections action = SessionsFragmentDirections.actionNavigationSessionsToSessionsCreateFragment();
        Navigation.findNavController(v).navigate(action);
    }



    class SessionViewHolder extends RecyclerView.ViewHolder {
        public TextView timeTextview;


        public SessionViewHolder(@NonNull View itemView) {
            super(itemView);

            timeTextview = itemView.findViewById(R.id.session_item_time_textview);

        }

        public void bindToSession(Session session) {
            timeTextview.setText(sdf.format(session.getTime().toDate()));
        }
    }

    class SessionAdapter extends FirestoreAdapter<SessionViewHolder> {

        SessionAdapter(Query query) {
            super(query);
        }

        @Override
        public SessionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new SessionViewHolder(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_session, parent, false));
        }

        @Override
        public void onBindViewHolder(SessionViewHolder viewHolder, int position) {
            DocumentSnapshot documentSnapshot = getSnapshot(position);
            final Session session = documentSnapshot.toObject(Session.class);

            final String SessionKey = documentSnapshot.getId();

            // Set click listener for the whole Session view
            viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(getContext(), "클릭한 세션의 아이디 : " + SessionKey, Toast.LENGTH_SHORT).show();

                    sessionsViewModel.getSessionRepository().selectedSession = session;
                    NavDirections action = SessionsFragmentDirections.actionNavigationSessionsToSessionsCreateFragment();
                    Navigation.findNavController(v).navigate(action);
                }
            });


            // Bind Session to ViewHolder
            viewHolder.bindToSession(session);
        }
    }



    @Override
    public void onStart() {
        super.onStart();
        if (mAdapter != null) {
            mAdapter.startListening();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAdapter != null) {
            mAdapter.stopListening();
        }
    }

    public String getUid() {
        return FirebaseAuth.getInstance().getCurrentUser().getUid();
    }

    public Query getQuery(FirebaseFirestore databaseReference) {
        return databaseReference.collection("sessions")
                .whereEqualTo("instructorId", getUid())
                .orderBy("time", Query.Direction.ASCENDING);
    }

}