package aok_remote_instructor.android.imlabworld.com.data.entity;

import com.google.firebase.Timestamp;

public class Session {

    String id;
    String instructorId;
    Timestamp time;

    public Session(String id, String instructorId, Timestamp time) {
        this.id = id;
        this.instructorId = instructorId;
        this.time = time;
    }

    public Session() {}

    public String getId() {
        return id;
    }

    public String getInstructorId() {
        return instructorId;
    }

    public Timestamp getTime() {
        return time;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setInstructorId(String instructorId) {
        this.instructorId = instructorId;
    }

    public void setTime(Timestamp time) {
        this.time = time;
    }
}
