package aok_remote_instructor.android.imlabworld.com.ui.sessions;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import aok_remote_instructor.android.imlabworld.com.R;
import aok_remote_instructor.android.imlabworld.com.data.entity.Session;
import aok_remote_instructor.android.imlabworld.com.databinding.FragmentSessionsShareBinding;

public class SessionsShareFragment extends Fragment {

    private FragmentSessionsShareBinding binding;
    private SessionsViewModel sessionsViewModel;
//    private Session session;

    public SessionsShareFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        sessionsViewModel = ViewModelProviders.of(getActivity()).get(SessionsViewModel.class);

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_sessions_share, container, false);
        View root = binding.getRoot();

        // 날짜, 시간 텍스트뷰에 옵저버 달아서 파이어베이스값 실시간으로 받아오기
//        sessionsViewModel.subscribe().observe(getActivity(), new Observer<Session>() {
//            @Override
//            public void onChanged(Session session) {
//                binding.setSession(session);
//                Log.d("testt","변화감지");
//            }
//        });

        // 뷰모델의 세션을 프레그먼트에 가져옴
//        session = sessionsViewModel.getSession().getValue();
//        Log.e("testt", "생성된 세션 : " + session);


        final ImageView backArrow = binding.fragmentSessionsShareBackArrow;
        backArrow.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                NavDirections action = SessionsShareFragmentDirections.actionSessionsEditFragmentToSessionsCreateFragment();
                Navigation.findNavController(v).navigate(action);
            }

        });

        final String link = "imlabworld.com/";// + session.getId();

        final LinearLayout shareLinkLinearLayout = binding.fragmentSessionsShareShareLinearlayout;
        shareLinkLinearLayout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, "Hi, ... \n\n Your CPR Class will be held on ... at ... \n\n Your class link is : \n" + link);
                sendIntent.setType("text/plain");

                Intent shareIntent = Intent.createChooser(sendIntent, null);
                startActivity(shareIntent);

            }

        });

        final TextView sessionLinkTextview = binding.fragmentSessionsShareLinkTextview;
        sessionLinkTextview.setText(link);
        sessionLinkTextview.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                ClipData clipData = ClipData.newPlainText("link",link);
                ClipboardManager cm = (ClipboardManager)getContext().getSystemService(Context.CLIPBOARD_SERVICE);
                cm.setPrimaryClip(clipData);
                Toast.makeText(getContext(), "클립보드에 저장되었습니다.", Toast.LENGTH_SHORT).show();
            }

        });



        return root;
    }
}