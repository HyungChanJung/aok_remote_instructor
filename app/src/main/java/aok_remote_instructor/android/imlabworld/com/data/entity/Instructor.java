package aok_remote_instructor.android.imlabworld.com.data.entity;

public class Instructor {
    String id;
    String username;
    String email;

    public Instructor(String id, String username, String email) {
        this.id = id;
        this.username = username;
        this.email = email;
    }

    public Instructor () {}

    public String getUsername() {
        return username;
    }

    public String getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}


