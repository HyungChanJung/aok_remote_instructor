package aok_remote_instructor.android.imlabworld.com.ui.sessions;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import org.webrtc.SurfaceViewRenderer;

import java.util.ArrayList;
import java.util.List;

import aok_remote_instructor.android.imlabworld.com.R;
import aok_remote_instructor.android.imlabworld.com.data.entity.Participant;

public class ParticipantAdapter extends RecyclerView.Adapter<ParticipantAdapter.ParticipantViewHolder> {
    private List<String> mParticipantIds = new ArrayList<>();
    private List<Participant> mParticipants = new ArrayList<>();

    private ListenerRegistration listenerRegistration;

    public ParticipantAdapter(Query query) {
        // Create child event listener
        // [START child_event_listener_recycler]
        EventListener childEventListener = new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot snapshots,
                                @Nullable FirebaseFirestoreException e) {
                if (e != null) {return;}
                String participantKey;
                int participantIndex;
                Participant participant;

                for (DocumentChange dc : snapshots.getDocumentChanges()) {
                    switch (dc.getType()) {
                        case ADDED:
                            // A new participant has been added, add it to the displayed list
                            participant = dc.getDocument().toObject(Participant.class);
                            // [START_EXCLUDE]
                            // Update RecyclerView
                            mParticipantIds.add(dc.getDocument().getId());
                            mParticipants.add(participant);
                            notifyItemInserted(mParticipants.size() - 1);
                            break;
                        case MODIFIED:
                            // A participant has changed, use the key to determine if we are displaying this
                            // participant and if so displayed the changed participant.
                            participant = dc.getDocument().toObject(Participant.class);
                            participantKey = dc.getDocument().getId();
                            // [START_EXCLUDE]
                            participantIndex = mParticipantIds.indexOf(participantKey);
                            if (participantIndex > -1) {
                                // Replace with the new data
                                mParticipants.set(participantIndex, participant);

                                // Update the RecyclerView
                                notifyItemChanged(participantIndex);
                            } else {
                                Log.w("testt", "onChildChanged:unknown_child:" + participantKey);
                            }
                            // [END_EXCLUDE]
                            break;
                        case REMOVED:
                            // A participant has changed, use the key to determine if we are displaying this
                            // participant and if so remove it.
                            participantKey = dc.getDocument().getId();
                            // [START_EXCLUDE]
                            participantIndex = mParticipantIds.indexOf(participantKey);
                            if (participantIndex > -1) {
                                // Remove data from the list
                                mParticipantIds.remove(participantIndex);
                                mParticipants.remove(participantIndex);

                                // Update the RecyclerView
                                notifyItemRemoved(participantIndex);
                            } else {
                                Log.w("testt", "onChildRemoved:unknown_child:" + participantKey);
                            }
                            // [END_EXCLUDE]
                            break;
                    }
                }

            }
        };
        // [END child_event_listener_recycler]
        listenerRegistration = query.addSnapshotListener(childEventListener);
    }

    @Override
    public ParticipantViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.item_participant, parent, false);
        return new ParticipantViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ParticipantViewHolder holder, int position) {
        Participant participant = mParticipants.get(position);
        holder.nameTextview.setText(participant.getName());
        holder.stateTextview.setText(participant.getState());
    }

    @Override
    public int getItemCount() {
        return mParticipants.size();
    }

    public void cleanupListener() {
        listenerRegistration.remove();
    }

    public class ParticipantViewHolder extends RecyclerView.ViewHolder {

        public TextView nameTextview;
        public TextView stateTextview;
        SurfaceViewRenderer localVideoView, friendVideoView;

        ParticipantViewHolder(View itemView) {
            super(itemView);

        }
    }


}
