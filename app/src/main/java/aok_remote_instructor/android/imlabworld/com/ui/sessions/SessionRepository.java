package aok_remote_instructor.android.imlabworld.com.ui.sessions;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.Timestamp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.SetOptions;

import aok_remote_instructor.android.imlabworld.com.data.entity.Instructor;
import aok_remote_instructor.android.imlabworld.com.data.entity.Session;

public class SessionRepository {

    private FirebaseFirestore db;

    private MutableLiveData<Session> sessionMutableLiveData = new MutableLiveData<>();

    Session selectedSession;

    public SessionRepository() {
        db = FirebaseFirestore.getInstance();
    }

    public FirebaseFirestore getDb() {
        return db;
    }

    public LiveData<Session> getSessionMutableLiveData() { return sessionMutableLiveData; }

    public void createSession(final Timestamp timestamp) {

        final String userId = getUid();
        DocumentReference instructorDocRef = db.collection("users").document(userId);
        instructorDocRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                Instructor user = documentSnapshot.toObject(Instructor.class);
                if (user == null) {
                    Log.e("testt", "User " + userId + " is unexpectedly null");
                } else {

                    // 레포지토리에 세션변수는 지금 선택된 세션이 있는지를 판단하게함
                    // 따라서 sessionCreateFragment의 onDestroy에서 지워주면 됨
                    if (selectedSession == null) {
                        DocumentReference docRef = db.collection("sessions").document();
                        selectedSession = new Session(docRef.getId(), userId, timestamp);
                        docRef.set(selectedSession, SetOptions.merge());
                        sessionMutableLiveData.setValue(selectedSession);
                        Log.e("testt", "SessionRepository : 생성된 세션 : " + selectedSession);
                    } else {
                        DocumentReference docRef = db.collection("sessions").document(selectedSession.getId());
                        selectedSession = new Session(docRef.getId(), userId, timestamp);
                        docRef.set(selectedSession, SetOptions.merge());
                        sessionMutableLiveData.setValue(selectedSession);
                        Log.e("testt", "SessionRepository : 수정된 세션 : " + selectedSession);
                    }

                }

            }
        });
    }

    public void updateSession(Timestamp timestamp) {
//        db.collection("sessions").document(sessionId).set(session, SetOptions.merge());
    }

    public String getUid() {
        return FirebaseAuth.getInstance().getCurrentUser().getUid();
    }
}
