package aok_remote_instructor.android.imlabworld.com.data.entity;

public class Participant {
    String studentId;
    String dataId;
    String name;
    String email;
    String state;
    String depth;
    String cpm;
    int score;

    public Participant(String studentId, String dataId, String name, String email, String state, String depth, String cpm, int score) {
        this.studentId = studentId;
        this.dataId = dataId;
        this.name = name;
        this.email = email;
        this.state = state;
        this.depth = depth;
        this.cpm = cpm;
        this.score = score;
    }

    public Participant () {}

    public String getStudentId() {
        return studentId;
    }

    public String getDataId() {
        return dataId;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getState() {
        return state;
    }

    public String getDepth() {
        return depth;
    }

    public String getCpm() {
        return cpm;
    }

    public int getScore() {
        return score;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public void setDataId(String dataId) {
        this.dataId = dataId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setState(String state) {
        this.state = state;
    }

    public void setDepth(String depth) {
        this.depth = depth;
    }

    public void setCpm(String cpm) {
        this.cpm = cpm;
    }

    public void setScore(int score) {
        this.score = score;
    }
}
