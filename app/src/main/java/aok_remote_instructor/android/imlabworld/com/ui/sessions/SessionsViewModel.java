package aok_remote_instructor.android.imlabworld.com.ui.sessions;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.firebase.Timestamp;

import aok_remote_instructor.android.imlabworld.com.data.entity.Session;

public class SessionsViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    // session liveData 선언, 읽기만 가능, 쓰기는 Repository에 있는 mutableLiveData이용
    private LiveData<Session> sessionLiveData;
    private SessionRepository sessionRepository;

    //레포지토리를 직접 호출할게 아니라 뷰모델을 통해서 데이터를 가져오는것이 바람직함
    public SessionRepository getSessionRepository() {
        return sessionRepository;
    }

    public SessionsViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is sessions fragment");

        sessionRepository = new SessionRepository();
        sessionLiveData = sessionRepository.getSessionMutableLiveData();
    }

    public LiveData<String> getText() {
        return mText;
    }

    public LiveData<Session> getSession() { return sessionLiveData; }

    public void setSession(LiveData<Session> sessionLiveData) {
        this.sessionLiveData = sessionLiveData;
    }

    public void createSession(Timestamp timestamp) {
        sessionRepository.createSession(timestamp);
    }

    public void updateSession(Timestamp timestamp) {
        sessionRepository.updateSession(timestamp);
    }

}