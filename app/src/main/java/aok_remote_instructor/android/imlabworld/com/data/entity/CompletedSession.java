package aok_remote_instructor.android.imlabworld.com.data.entity;

import com.google.firebase.Timestamp;

public class CompletedSession {

    String id;
    String sessionId;
    Timestamp time;

    public CompletedSession(String id, String sessionId, Timestamp time) {
        this.id = id;
        this.sessionId = sessionId;
        this.time = time;
    }

    public CompletedSession () {}

    public String getId() {
        return id;
    }

    public String getSessionId() {
        return sessionId;
    }

    public Timestamp getTime() {
        return time;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public void setTime(Timestamp time) {
        this.time = time;
    }
}
